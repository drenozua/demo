package com.agile.parser;

import com.agile.parser.service.HtmlParserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

import static java.lang.System.out;

@SpringBootApplication
public class ApplicationMain {

    private static final String SEPARATOR_CHARACTER = "#";
    private static final int MIN_SEPARATOR_LENGHT = 20;
    private static final String DEFINITION_MESSAGE = "CSS selectors";

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ApplicationMain.class, args);
        HtmlParserService service = context.getBean(HtmlParserService.class);
        List<String> selectors = service.finSelectorFromFiles(args);

        printSelectors(selectors);
    }


    private static void printSelectors(final List<String> selectorList) {
        final int separatorLenght = selectorList.stream().mapToInt(String::length).max().orElse(MIN_SEPARATOR_LENGHT);
        final String separatorRow = getSeparator(separatorLenght);
        final String emptySeparatorRow = getEmptySeparator(separatorLenght);
        final String definitionRow = getDefinitionMessage(separatorLenght);

        out.println(separatorRow);
        out.println(emptySeparatorRow);
        out.println(definitionRow);
        out.println(emptySeparatorRow);
        out.println(separatorRow);
        out.println(emptySeparatorRow);
        selectorList.stream().map(i -> "# " + i + " #").forEach(out::println);
        out.println(emptySeparatorRow);
        out.println(separatorRow);
        out.println();
    }

    private static String getDefinitionMessage(int separatorLenght) {
        int prefixLenght = (separatorLenght - DEFINITION_MESSAGE.length()) / 2;
        return SEPARATOR_CHARACTER
                + StringUtils.repeat(" ", prefixLenght)
                + DEFINITION_MESSAGE
                + StringUtils.repeat(" ", separatorLenght - prefixLenght - DEFINITION_MESSAGE.length() + 2)
                + SEPARATOR_CHARACTER;

    }

    private static String getEmptySeparator(int lenght) {
        return SEPARATOR_CHARACTER
                + StringUtils.repeat(" ", lenght + 2)
                + SEPARATOR_CHARACTER;
    }

    private static String getSeparator(int lenght) {
        return StringUtils.repeat(SEPARATOR_CHARACTER, lenght + 4);
    }
}
