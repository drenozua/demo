package com.agile.parser.service;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class HtmlParserServiceImp implements HtmlParserService {

    @Value("${ok-button.css.selector}")
    private String okButtonSelector;

    public Optional<Document> getDoc(final String link) {
        Document doc = null;

        try {
            Connection connection = Jsoup.connect(link);
            doc = connection.get();
        } catch (IOException e) {
            log.error("Can't connect to the resource: {}. {}", link, e.getMessage());
        }

        return Optional.ofNullable(doc);
    }

    public Optional<Document> getDoc(final File file) {
        Document doc = null;

        try {
            doc = Jsoup.parse(file, StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            log.error("Can't connect to the resource: {}. {}", file, e.getMessage());
        }

        return Optional.ofNullable(doc);
    }

    public Optional<Elements> parseBySelector(final String id, Document doc) {
        return Optional.ofNullable(doc.select(id));
    }

    public Optional<String> getFirstSelector(final Elements elements) {
        return elements.stream()
                .map(Element::cssSelector)
                .filter(Objects::nonNull)
                .findFirst();
    }

    public String findSelector(final File link) {
        return getDoc(link).flatMap(doc -> parseBySelector(okButtonSelector, doc))
                .flatMap(this::getFirstSelector)
                .orElse("undefined");
    }

    public String findSelector(final String link) {
        return getDoc(link).flatMap(doc -> parseBySelector(okButtonSelector, doc))
                .flatMap(this::getFirstSelector)
                .orElse("undefined");
    }

    public List<String> finSelectorFromFiles(final String... fileNames) {
        return Arrays.stream(fileNames)
                .map(File::new)
                .filter(File::exists)
                .map(this::findSelector)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
