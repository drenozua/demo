package com.agile.parser.service;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.List;
import java.util.Optional;

public interface HtmlParserService {
    Optional<Document> getDoc(String link);

    Optional<Document> getDoc(final File file);

    Optional<Elements> parseBySelector(String id, Document doc);

    Optional<String> getFirstSelector(final Elements elements);

    String findSelector(final String link);

    String findSelector(final File link);

    List<String> finSelectorFromFiles(final String... fileNames);
}
