package com.agile.parser.service;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.jupiter.api.RepeatedTest;

import java.util.Optional;

import static junit.framework.TestCase.assertEquals;

public class HtmlParserServiceImpTest {

    public static final int REPEAT_COUNT = 10;

    private HtmlParserServiceImp service = new HtmlParserServiceImp();
    private String selector = "#page-wrapper > div:nth-child(3) > div.col-lg-8 > div > div a.btn:not(.btn-warning):not( .btn-danger)";

    private Optional<Document> document;

    @Test
    @RepeatedTest(REPEAT_COUNT)
    public void parseByIdFirst() {
        String link = "https://agileengine.bitbucket.io/keFivpUlPMtzhfAy/samples/sample-1-evil-gemini.html";
        document = service.getDoc(link);
        document.ifPresent(doc -> {
            Optional<Elements> elements = service.parseBySelector(selector, doc);

            assertEquals("Element's not exist", elements.filter(i -> i.size() != 0).isPresent(), true);
            assertEquals("Element not one", elements.filter(i -> i.size() == 1).isPresent(), true);
        });
    }

    @Test
    @RepeatedTest(REPEAT_COUNT)
    public void parseByIdSecond() {
        String link = "https://agileengine.bitbucket.io/keFivpUlPMtzhfAy/samples/sample-3-the-escape.html";
        document = service.getDoc(link);
        document.ifPresent(doc -> {
            Optional<Elements> elements = service.parseBySelector(selector, doc);

            assertEquals("Element's not exist", elements.filter(i -> i.size() != 0).isPresent(), true);
            assertEquals("Element not one", elements.filter(i -> i.size() == 1).isPresent(), true);
        });
    }

    @Test
    @RepeatedTest(REPEAT_COUNT)
    public void parseByIdThird() {
        String link = "https://agileengine.bitbucket.io/keFivpUlPMtzhfAy/samples/sample-4-the-mash.html";
        document = service.getDoc(link);
        document.ifPresent(doc -> {
            Optional<Elements> elements = service.parseBySelector(selector, doc);

            assertEquals("Element's not exist", elements.filter(i -> i.size() != 0).isPresent(), true);
            assertEquals("Element not one", elements.filter(i -> i.size() == 1).isPresent(), true);
        });
    }
}
